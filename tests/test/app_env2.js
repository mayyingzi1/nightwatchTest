var returnEnv = require('../testGlobal/returnEnv')
returnEnv = returnEnv.returnEnv()
var setToken = {
	'product':'ded61038593734d3b1ad4fb667f7bce5',
	'alpha':'3a19b4561ecf694f7a5cfa5b81e471e4',
	'beta': '5f615194965e1c090e6d38fe4f7b2bc8'
}
module.exports = {
	'@disabled': true,
	'@tags':['appEnv'],
	before: function(client){
		client.timeoutsAsyncScript(3000);
  		client.resizeWindow(414, 736)
		// 当前测试模块clinet.currentTest.name
		// console.log(client)
		console.log('runner is starting')


	},
	after: function(client){
		console.log('runner is ending')


	},
	beforeEach: function (client){
		//console.log(client.currentTest.name+':is starting')
	},
	afterEach: function(client,done){
		//console.log(client.currentTest.name+':is ending')
		done()

	},
	'商城app：从url中获取token，（注意,没有 token_type），且能够正确判断 appName 为 imall': function(client){
		var browserUrl = '#index/~token='+setToken[returnEnv]
		client.urlHash(browserUrl).execute(function(data){
			try{
				localStorage.pepe = 1
			} catch(e) {
				console.log(e);
			}
			return localStorage.getItem('userInfo')
		},[],function(result){
			console.log(result.value)
			var UseInfo = JSON.parse(result.value)
			this.assert.equal(UseInfo.token_type, undefined,'商城app跳转h5不传递token_type')
			this.assert.equal(UseInfo.token, setToken[returnEnv],'验证商城app传递的token是否存储到localstorage')
	        client.assert.cssClassPresent("body", "fc_platform_browser",'商城app跳转h5页面body添加样式类fc_platform_browser');
		})
	},
	'设置 当前设备为Android': function(client){
		var appHash = 'index'
		var appUrl = '#'+appHash+'/~token='+setToken[returnEnv]
		client.urlHash(appUrl).execute(function(){
			window.detectDevice('android')
		},[], function(result){
			client.assert.cssClassPresent("body", 'fc_platform_android','安卓app环境检测body是否有样式类fc_platform_android');
			client.assert.cssClassPresent("body", 'fc_app_imall','安卓app环境检测body是否有样式类fc_platform_android');
			// 点击跳转 url不改变
			client.click('.app-box .nav-user-center')
			client.assert.urlContains(appHash,'安卓app环境点击用户中心，失效，依然在当前页面');
		})
    // client.end()
	}
}
