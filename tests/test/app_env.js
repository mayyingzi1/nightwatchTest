var returnEnv = require('../testGlobal/returnEnv')
returnEnv = returnEnv.returnEnv()
var setIcarToken = {
	'product':'e9296b509a2a38bfc86eea1e77074cd2',
	'alpha':'3a19b4561ecf694f7a5cfa5b81e471e4',
	'beta': '6b814156a640789f3e50bf264d52e731'
}
module.exports = {
	'@disabled': true,
	'@tags':['appEnv'],
	before: function(client){
		client.timeoutsAsyncScript(3000);
  		client.resizeWindow(414, 736)
		// 当前测试模块clinet.currentTest.name
		// console.log(client)
		console.log('runner is starting')


	},
	after: function(client){
		console.log('runner is ending')


	},
	beforeEach: function(client){
		//console.log(client.currentTest.name+':is starting')
	},
	afterEach: function(client,done){
		//console.log(client.currentTest.name+':is ending')
		done()

	},
	'微信 icar： 从 url 中获取 token，且能正确判断 环境为 icar': function(client){
		var weixinUrl = '#index/~token='+setIcarToken[returnEnv]+'&code=null&token_type=icar'
		client.urlHash(weixinUrl).execute(function(data){
	       try {
	            // statements
	            localStorage.pepe = 1
	            // console.log('local', localStorage.getItem('userInfo'))
	        } catch(e) {
	            // statements
	            console.log(e);
	        }
	      // ☆返回布尔值或者对象
	      return localStorage.getItem('userInfo')
	      // ☆ 如果返回字符串 result.value的值将会是undefined
	      // return localStorage.getItem('userInfo').user_name
	    },[], function(result){
	      if(result){
	      console.log(result.value)
	      	var UseInfo = JSON.parse(result.value)
	      	// 验证：
	      	this.assert.equal(UseInfo.token_type, 'icar','获取localstorage中用户信息token_type=icar')
	        client.assert.cssClassPresent("body", "fc_app_icar_h5",'icar跳转body添加样式fc_app_icar_h5');
	      }

	    })
    // client.end()
	}
}
