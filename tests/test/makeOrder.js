/**
 * Created by zhuxy on 2016/9/18.
 * test process make a Order
 */
var _ = require('underscore');
var request = require('request')
var URL = require('../testGlobal/URL')
var orderConfig = {
    'userForm' : {
        'name': '15078544078',
        'psw': '15078544078'
    },
    'good': {
        'searchKey': '1111',
        'addNum': '2'

    }
}
var orderGoods = { }
module.exports = {
    '@disabled': false,
    '@tags': ['makeOrder'],
    before: function (client) {
        console.log('runner start============')
        client.timeoutsAsyncScript(3000);
        client.resizeWindow(414, 736);console.log('runner start============')
        client.timeoutsAsyncScript(3000);
        client.resizeWindow(414, 736);
    },
    after: function (client) {
        console.log('runner end============')
    },
    beforeEach: function (client){
        console.log('per starting============')
    },
    afterEach: function (client, done) {
        console.log('per ending============')
        done()
    },
    'search a good by keywords ': function (client) {
        client
            .urlHash('hotsearch')
            .waitForElementVisible('.search-input-box' ,5000)
            .execute(function (data) {
                var inpitObj = $(".search-input-box");
                // 清空原始的input 的value值
                // 修复 setValue直接赋值是原始value不会清空bug（这个bug不会在ios出现）
                inpitObj.val('');
            }, [], function () {

            });
        client
            .setValue('.search-input-box', orderConfig.good.searchKey)
            .pause(2000)
            .click('.search-nav-btn')
            .pause(2000)
        var itemP = '.all ul li:first-child';
        var itemPLink = '.all ul li:first-child a';
        // 获取即将点击的商品链接信息
        client
            .getAttribute('.all ul li:first-child a', 'href', function (result) {
                var Link = URL.parse(result.value.split('~')[1]);
                orderGoods.goods_id = Link.goods_id
            })
            .pause(2000)
      client
          .elements('css selector', '.goods-list-tpl .item', function (result) {
              var itemLen = result.value.length
              if (!itemLen) {
                  // 无搜索结果校验
                  client
                      .assert.elementPresent('.goods-list-tpl .tips:first-child', '无结果显示UI提示')
                      .assert.containsText('.goods-list-tpl .tips:first-child', '很抱歉，目前暂时找不到' + orderConfig.good.searchKey + '的商品搜索结果' + '\n'+'请重新搜索', '无结果文案匹配')
                      .saveScreenshot('reports/search_null.png')
                      .pause(5000)
                      .execute(function (data) {
                          var inpitObj = $(".search-input-box");
                          // 清空原始的input 的value值
                          // 修复 setValue直接赋值是原始value不会清空bug（这个bug不会在ios出现）
                          inpitObj.val('');
                      }, [], function () {})
                      .click('.search-nav-btn')
              }
          })
          .waitForElementVisible(itemP, 2000, '搜索成功类表展示')
          .click(itemP)
          .pause(2000)
          .waitForElementVisible('.page-view-product-detail',5000,'商品详情页面加载成功')
          .pause(2000)
    },
    '商品详情——微信环境UI校验': function (clinet) {
       clinet
           .refresh()
           .executeAsync(function (done) {
               // 设置当前环境是WeChat
               window.currentDevice = 'wechat'

       }, [], function () {
       })
        // 检验分享按钮显示
        clinet
            .assert.elementPresent('.head-btn-group .btn-share','微信环境分享按钮显示')
            .pause(2000)
            .saveScreenshot('reports/product_weixin_ui.png')
            .pause(8000)
    },
    '立即购买用户校验': function(client){
        // 获取当前用户信息 如果用户信息存在，则出现属性选择面板，否则，进入登录页面
      client
          .refresh()
          .waitForElementVisible('.goto-buy-btn' ,5000)
          .executeAsync ( function (done) {
          // 恢复UI设置 browser
          window.currentDevice = 'browser'
          return done(localStorage.getItem('userInfo'))
        }, [], function (result) {
          var goUrl='';
          var isLogin = false
          var userInfo =JSON.parse(result.value)
          if(_.has(userInfo, 'token')){
              // 如果已经登录，则直接进入购买
              goUrl='product'
              orderGoods.userInfo = userInfo
          }else{
                goUrl = 'login'
                isLogin = true
          }
        client
            .pause(2000)
            .assert.cssProperty('.head-btn-group .btn-share',"display", "none", '非微信环境分享按钮不显示')
            .click('.goto-buy-btn')
            .pause(2000)
            .assert.urlContains(goUrl,'跳转页面校验url是否存在：'+goUrl);
        if(isLogin){
            var LoginIn = require('../testGlobal/commonLogin')
            LoginIn.loginIn(client,orderConfig.userForm.name,orderConfig.userForm.psw)
        }
        })
      client
          .waitForElementVisible('.goto-buy-btn' ,5000)
          .assert.urlContains('#product/~goods_id=','常规商品详情url匹配：#product/~goods_id=')
    },
    '点击“立即购买”UI属性面板检测': function(client){
        var confirmBtn = '.confirm-goods:last-child'
        client
            .click('.goto-buy-btn')
            .pause(2000)
            .assert.cssProperty(".select-product-box", "display", "block",'显示属性面板')
            .assert.containsText(confirmBtn, "确定",'立即购买对应属性面买的确认按钮文案：确定')
            .elements('css selector','.product-property .select-group', function(result){
             var groupLen = result.value.length
              if(groupLen){
                  client
                      .executeAsync(function(done){
                          var obj = $('.product-property .select-group')
                          obj.map(function(index, ele){
                              var _obj = obj.eq(index).find('.select')
                              // console.log(index)
                              // console.log('查找对应的元素')
                              // console.log(_obj)

                              if(_obj.eq(0).hasClass('.select-disabled')){
                                  // 第一个不可选
                                  _obj.eq(1).click()
                              }else{
                                  // 选择属性的第一个
                                  _obj.eq(0).click()
                              }
                          })
                          done()
                      },[],function(result){
                          // client
                          //     .click(confirmBtn)

                      })
              }

            })
            .elements('css selector','.product-property-box .install-box', function(result){
                var installBoxLen = result.value.length
                if(installBoxLen){
                    // 获取被选中安装属性的值
                    client
                        .getAttribute('.install-box.show-install .selected', 'data-install', function (result) {
                            orderGoods.to_sstore = result.value;
                        })
                }
            })
        client
            .getValue('.good-amount', function (result) {
                orderGoods.quantity = result.value
            })
        client
            .executeAsync ( function (done) {
                return done(localStorage.getItem('userInfo'))
                }, [], function (result) {
                    var userInfo = JSON.parse(result.value)
                    if (_.has(userInfo, 'token')) {
                        var formData={};
                        formData.goods_id =orderGoods.goods_id
                        formData.token = userInfo.token
                        // request.post(
                        //     {
                        //         url:'http://api.mall.carisok.com/mallapp.php/product/get_goods_specs',
                        //         formData: formData
                        //     },
                        //     function optionalCallback(err, httpResponse, body) {
                        //         if (err) {
                        //             return console.error('upload failed:', err);
                        //         }
                        //         console.log(httpResponse.response)
                        //         console.log('Upload successful!  Server responded with:', body);
                        //     })
                        request({
                                method: 'POST',
                                formData: formData,
                                uri: 'http://api.mall.carisok.com/mallapp.php/product/get_goods_specs',
                            },
                            function (error, response, body) {
                                if (error) {
                                    return console.error('upload failed:', error);
                                }
                                console.log(response)
                                console.log('Upload successful!  Server responded with:', body);
                            })
                    }
                })

        client
            .pause(1000000)
            .click(confirmBtn)
            .pause(3000)

    },
    '是否选择安装店': function (client) {
        client
            .pause(2000)
            .elements('css selector','.product-property .select-group',function(result){
                var sstoreItem = result.value.length;
                if(sstoreItem){
                    client
                        .assert.urlContains('/#searchsstore/~spec_id=10066&quantity=1&sstore_license_id=1&channel_source=undefined&source_id=undefined&to_sstore=1', '进入"选择枫车安装店"')
                }


            })
            .executeAsync(function (done) {

            }, [], function () {

            })
        
    },
    '确认订单页面': function (client) {
        client
            .waitForElementVisible('.shopping-confirmorder',2000)
            .assert.urlContains('#confirmorder/','确认订单url校验匹配：#confirmorder/')
    }
}
