var nightwatch = require('nightwatch');
var process = require('process')
var nowEnv = process.argv[3] ? process.argv[3].replace(/-/g, '') : 'product'


var configs = {
  accountConfig:{
    username: '15078544078',
    password: '15078544078'
  },
  containContian:{
    local:'http://localhost:3111/',
    alpha: 'http://192.168.1.206/imall/',
    beta:'http://test.carisok.com/imall/',
    product:'http://imall.carisok.com/'
  },
  urlRouter:{
    index: '',
    userCenter:'#usercenter',
    shoppingCart:'#shoppingcart',

  },
  setEnv: function(evn){
      if(evn === 'local'){
        return 'local'
      }else if(evn === 'alpha'){
        return 'alpha'
      }else if(evn === 'beta') {
        return 'beta'
      }else if( evn === 'product') {
        return 'product'
      }else{
        return 'product'
      }
  }

}

var getEnv = configs.containContian[configs.setEnv(nowEnv)];

module.exports = {
    before : function(browser,done) {

      setTimeout(function(){
        console.log('Setting up...');

        done()
      },30)
    //console.log('Setting up...');
  },

  after : function(browser) {
    console.log('Closing down...');

  },

  beforeEach : function(browser) {
    console.log('show beforeEach')

  },

  afterEach : function() {
    console.log('show afterEach')

    },
  // To select which tags to run, use the --tag
  // eg:--tag login --tag something_else
  // To skip running tests with a specific tag, use the --skiptags
  // eg:--skiptags login,something_else
  "@tags":['login','search', 'order','url'],
  "@disabled": true,
  // 'change url': function(client){
  //     // change url
  //     client.urlHash('#usercenter');
  //     // client.pause(3000)
  //     // client.urlHash('#usercenter').execute(function(data) {


  //       client.execute(function(data) {

  //       try {
  //           // statements
  //           localStorage.pepe = 1
  //           console.log('local', localStorage)
  //       } catch(e) {
  //           // statements
  //           console.log(e);
  //       }
  //       return true;
  //   }, [], function(result) {
  //   });

  // },
  'imall login': function (client) {

    var navCenter = '.app-box .nav-user-center'
    // client.url('http://imall.carisok.com/').maximizeWindow()
    // client.url(getEnv+configs.urlRouter.index)
    client.resizeWindow(414, 736);
    client.urlHash(configs.urlRouter.index)
    //console.log(client.navigate())


    // Page Init.
    client.expect.element('body').to.be.present.before(3000)
    client.waitForElementVisible(navCenter, 2000, function(){
      console.log()
      client.assert.containsText('.entrance-item:last-child p','换机油')
    })
    // 检测内容是否正确

    var firstPage = client.pages

    client.pause(2000)
    client.click(navCenter)

    // client.waitForAjaxCompleted(function(){
    //     console.log('zhuxaioying')
    // });
    // client.waitForElementVisible('.user-info', 5000)
    // client.waitForCondition('.user-info',5000,function(result){
    //     console.log(result)
    // });
    // 获取url信息
    client.assert.urlContains('usercenter');

    // Login.
    client.waitForElementVisible('.btn-login', 5000)
    client.click('.btn-login')
    client.waitForElementVisible('.user-name', 2000)
    client.pause(2000)
    client.setValue('.user-name', configs.accountConfig.username)
    client.setValue('.user-password', configs.accountConfig.password)
    client.click('.submit-btn', function(result){
         // console.log(result)
    })
    client.pause(5000)
    // check login msg
    // 在execute 的回调函数中获取window中的数据操作也是基于浏览器的
    // 包括localstorage、cookie等
    var UseInfo= {}
    client.urlHash('#usercenter').execute(function(data){
      var nowUseInfo = localStorage.getItem('userInfo').user_name;
       try {
            // statements
            localStorage.pepe = 1
            console.log('local', localStorage.getItem('userInfo'))
        } catch(e) {
            // statements
            console.log(e);
        }
      // ☆返回布尔值或者对象
      return localStorage.getItem('userInfo')
      // ☆ 如果返回字符串 result.value的值将会是undefined
      // return localStorage.getItem('userInfo').user_name
    },[], function(result){
      if(result){

        // console.log(result.value)
        UseInfo = JSON.parse(result.value)
        client.assert.containsText('.page-view-usercenter .user-name', UseInfo.user_name)
        client.saveScreenshot('reports/imall_login2.png')
      }


    })

    // Take a screenshot and save to "reports/imall_login.png".
    //client.saveScreenshot('reports/imall_login.png')
    client.pause(1000)

  },
  'show search ': function(client){
      client.url(getEnv+configs.urlRouter.index)
      client.expect.element('body').to.be.present.before(3000)

      client.expect.element('.search').to.be.visible

      // search
      client.waitForElementVisible('.search',8000)
      client.click('.search')
      client.pause(2000)
      client.setValue('.search-input-box', '轮胎')
      client.click('.search-nav-btn')
      client.pause(2000)
      // Take a screenshot and save to "reports/imall_login.png".
      client.saveScreenshot('reports/imall_search.png')
      client.pause(1000)

      // sort product
      // tab=sall
      client.click('.tab-list .tab.sall')
      client.pause(2000)
      client.saveScreenshot('reports/imall_tabSall.png')

      // tab=price
      client.click('.tab-list .tab.price')
      client.pause(2000)
      client.saveScreenshot('reports/imall_tabPrice1.png')

      // tab=price
      client.click('.tab-list .tab.price')
      client.pause(2000)
      client.saveScreenshot('reports/imall_tabPrice2.png')

      // go product-detail
      var itemP = '.price-asc ul li:first-child'
      client.waitForElementVisible(itemP,2000)
      client.click(itemP)
      client.pause(2000)
      client.waitForElementVisible('.page-view-product-detail',5000)
      client.saveScreenshot('reports/imall_product.png')
      client.pause(2000)
  },
  'make a order': function(client){
      client.click('.shopping-bar .buy-now')
      client.pause(3000)
      client.waitForElementVisible('.placeholder-detail-property',5000)
      client.saveScreenshot('reports/imall_detail_property.png')
      client.click('.select-group:last-child .select:first-child')
      client.click('.placeholder-detail-property .confirm-goods')

  }


}
