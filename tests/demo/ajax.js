// var assert = require('assert');
module.exports = {
'@disabled': true,
 'vue-router e2e tests': function (browser) {
    browser
    // default 404
    .url(base)
      .waitForElementVisible('h1', 1000)
      .assert.containsText('.view', 'FOUR OH FOUR')

    // /about
    .url(base + '/about')
      .waitForElementVisible('h1', 1000)
      .assert.containsText('.view h2', 'ABOUT US')
      .assert.cssClassPresent('a[href="/about"]', 'v-link-active')
      // should not be able to navigate to inbox
      .click('a[href^="/inbox"]')
      .pause(100)
      .getAlertText(function (text) {
        this.assert.ok(/navigate/.test(text.value))
      })
      .acceptAlert()
      // should not have changed
      .assert.containsText('.view h2', 'ABOUT US')
      .assert.cssClassPresent('a[href="/about"]', 'v-link-active')

    // /user
    .url(base + '/user/1234/profile/what')
      .waitForElementVisible('h1', 1000)
      .assert.containsText('.view h2', 'User yo')
      .assert.containsText('.view h3', 'user profile')
      .assert.containsText('.view p', '1234 what')
      .assert.cssClassPresent('a[href^="/user"]', 'v-link-active')
      // change params
      .execute(function () {
        router.go('/user/2345/profile/hey')
      })
      .assert.containsText('.view p', '2345 hey')
      // other routes
      .execute(function () {
        router.go('/user/2345/posts')
      })
      .assert.containsText('.view h3', 'user posts')
      .execute(function () {
        router.go('settings')
      })
      .assert.containsText('.view div', 'user settings')

    // inbox
    .url(base + '/inbox')
      .waitForElementVisible('h1', 1000)
      .assert.containsText('.view h2', 'inbox!')
      .assert.containsText('.view', 'default yo')

    .url(base + '/inbox/message/123')
      .waitForElementVisible('h1', 1000)
      .assert.containsText('.view div', 'Loading data')
      .assert.containsText('.view h2', 'inbox!')
      .pause(3000)
      .assert.containsText('.view div', 'message #123: Hello this is a message')
      // confirm navigation
      .click('a[href^="/user"]')
      .pause(100)
      .getAlertText(function (text) {
        this.assert.ok(/Are you sure/.test(text.value))
      })
      // cancel navigation
      .dismissAlert()
      // wait for possible transition
      .pause(1000)
      .assert.containsText('.view div', 'message #123: Hello this is a message')
      // then do it again
      .click('a[href^="/about"]')
      .pause(100)
      .acceptAlert()
      .pause(1000)
      .assert.containsText('.view h2', 'ABOUT US')

    // forbidden
    .url(base + '/forbidden')
      .waitForElementVisible('#app > p', 1000)
      .assert.containsText('#app > p', 'Authenticating')
      .pause(3000)
      .getAlertText(function (text) {
        this.assert.ok(/forbidden by a global before hook/.test(text.value))
      })
      .acceptAlert()
      .assert.hidden('#app > p')

    // redirect
    .url(base + '/info')
      .waitForElementVisible('h1', 1000)
      .assert.containsText('.view h2', 'ABOUT US')
      .assert.cssClassPresent('a[href="/about"]', 'v-link-active')
      .end()
  },
// before : function(client, callback) {
//     client.perform(function() {
//       client.globals.calls++;
//       callback();
//     });
//   },

//   beforeEach : function(client, callback) {
//     client.perform(function() {
//       setTimeout(function() {
//         client.globals.calls++;
//         callback();
//       }, 10);
//     });
//   },

//   demoTest : function (client) {
//     client.url('http://imall.carisok.com/')
//       client.assert.elementPresent('.nav-index')
//       //client.end();
//   },

//   demoTest2 : function (client) {
//     client.url('http://imall.carisok.com/')
//       client.assert.elementPresent('.nav-user-center')
//       //client.end();
//   },

//   afterEach : function(client, callback) {
//     client.perform(function() {
//       setTimeout(function() {
//         client.globals.calls++;
//         callback();
//       }, 10);
//     });
//   },

  after : function(client, callback) {
    
      client.customPerform(function() {
        client.globals.calls++;
      })
      client.perform(function() {
        setTimeout(function() {
          client.globals.calls++;
          callback();
        }, 10);
      });
  }
 // beforeEach : function(client, callback) {
 //    var testName = client.currentTest.name;
 //    console.log(testName);
 //    client.assert.deepEqual(testName, 'demoTest');
 //    callback();
 //  },

 //  before : function(client, callback) {
 //    var testName = client.currentTest.name;
 //    console.log(testName);
 //    client.assert.deepEqual(testName, '');
 //    callback();
 //  },

 //  demoTest : function (client,done) {

 //    var testName = client.currentTest.name;

 //    client.assert.ok('Test OK');
 //    client.assert.equal(1, 1);
 //    client.assert.equal(testName, 'demoTest');
 //    setTimeout(function () {
 //      done();
 //    }, 10);
 //    client.end();
 //  },

 //  afterEach : function(client, callback) {
 //    var testName = client.currentTest.name;
 //    var results = client.currentTest.results;
 //    console.log(testName);
 //    console.log(results);
 //    client.assert.equal(results.passed, 2);
 //    client.assert.equal(results.failed, 0);
 //    client.assert.equal(results.tests, 2);
 //    client.assert.ok('demoTest' in results.testcases);
 //    client.assert.equal(results.testcases.demoTest.assertions.length, 2);
 //    client.assert.deepEqual(testName, 'demoTest');
 //    callback();
 //  },

 //  after : function(client, callback) {
 //    var testName = client.currentTest.name;
 //    client.assert.deepEqual(testName, null);
 //    callback();
 //  }
// demoTestSync : function (test) {
//     assert.equal(0, 0);
//   },

//   demoTestAsync : function(test, done) {
//     setTimeout(function () {
//       done();
//     }, 10);
//   }
// tags: ['login', 'other'],
// demoTest: function (client) {
    
//       client.assert.equal(client.options.desiredCapabilities.name, 'Ajax')
//       client.url('http://imall.carisok.com/')
//       client.assert.elementPresent('.nav-user-center')
//       client.end()
//   }
// 'demo UnitTest' : function (client) {
    // client.url('http://imall.carisok.com/')
    // client.assert.elementsCount('#x-root', 1);
    // client.assert.ok('TEST');
    
    // client.get('http://imall.carisok.com/', function(result){
    //     console.log('the result:' + result)        
    // });
    // client.waitForUrl('http://imall.carisok.com/',3000);
    // client.waitForAjaxCompleted(function(){
    //   console.log('have finished')
    // });

  // }
  // 'do ajax': function (client) {
  //   client
  //   .timeoutsAsyncScript(15000) // set async exec timeout
  //   .click('.btn-submit') // ajax form submit
  //   .executeAsync(
  //       function(targetUrl, done){
  //           var nightwatchAjaxCallback = function(ajaxUrl) {
  //               if(ajaxUrl.indexOf(targetUrl) === 0) { // if url match notify script that xhr is done
  //                   done(true);
  //               }
  //           };

  //           if(!window.nightwatchAjaxInited) { // make sure ajaxComplete is attached only once

  //               window.nightwatchAjaxInited = true;

  //               $(document).ajaxComplete(function(e, res, req) {
  //                   nightwatchAjaxCallback(req.url);
  //               });
  //           }
  //       },
  //       ['/ajaxpath'], // expected xhr request path
  //       function(status){
  //           // executes once ajax is done
  //           client.verify.containsText('.entry', 'lorem ipsup...') // verify post is created
  //       }
  //   );
  // }
}
