module.exports = {
	'@disabled':true,
	'@tags':['executeAsync'],
	before: function(client){
		console.log('runner start。。。。')
	},
	after: function(client){
		console.log('runner end。。。。')
	},
	beforeEach: function(client){
		console.log('per starting。。。。')
	},
	afterEach: function(client){
		console.log('per ending。。。。')
	},
	'Demo asynchronous script' : function (client) {
  client.timeoutsAsyncScript(10000);
  client.url('http://stackoverflow.com/');

  // execute a piece of script asynchroniously
  client.execute(function(data) {
        window._asyncResult = undefined;
        setTimeout(function(){
          window._asyncResult = "abcde";
        }, 2000);
     }, ["1234"]);

   // execute a task while the asynchroniously script is running
   client.assert.title('Stack Overflow');

   // wait for the asynchronous script to set a result
   client.executeAsync(function(done) {
        (function fn(){
            if(window._asyncResult !== undefined)
              return done(window._asyncResult);
            setTimeout(fn, 30);
        })();
   }, [], function(result) {
     // evaluate the result
     client.assert.equal(result.value, "abcde");
   });

  client.end();
}
  
}