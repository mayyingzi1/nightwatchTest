var _ = require('underscore')
module.exports = {
    '@disabled': true,
    '@tags': ['executeAsyncLogin'],
    before: function (client) {
        console.log('runner start。。。。')
    },
    after: function (client) {
        console.log('runner end。。。。')
    },
    beforeEach: function (client){
        console.log('per starting。。。。')
    },
    afterEach: function (client, done) {
        console.log('per ending。。。。')
        done()
    },
    'Demo asynchronous login script' : function (client) {
        client.timeoutsAsyncScript(3000);

        client.resizeWindow(414, 736)
        client.urlHash('#usercenter')

        // execute a piece of script asynchroniously
        client.execute(function(data) {
            window.userInfo = undefined;
            setTimeout(function(){
                window.userInfo = {
                'user_name': '15078544078',
                'user_password': '15078544078'
            }
            // window.detectDevice('android')
        }, 2000);
        }, ["1234"]);

        // Login.
        client.waitForElementVisible('.btn-login', 5000)
        client.click('.btn-login', function () {
          client.assert.urlContains('login');
        })
        client.waitForElementVisible('.user-name', 2000)
        client.pause(2000)
        // wait for the asynchronous script to set a result
        client.executeAsync(function(done) {

        (function fn(){
          if (window.userInfo !== undefined) {
              return done(window.userInfo)
          }
          setTimeout(fn, 30);
        })();

        }, [], function(result) {
            client.setValue('.user-name', result.value.user_name)
            client.setValue('.user-password', result.value.user_password)
            client.click('.submit-btn')


        });
        client.pause(5000)
      // client.end();
    },
    '用户信息成功存储，token存在': function (client) {
      client.executeAsync ( function (done) {
          // var userInfo = localStorage.getItem('userInfo')
         // console.log(localStorage)
          return done(localStorage.getItem('userInfo'))
      }, [], function (result) {
        // 用户信息成功存储
        // console.log('存储用户信息')
        // console.log(result.value)
        var userInfo =JSON.parse(result.value)
        client.assert.equal( _.has(userInfo, 'token'), true);
      })
    },
  '注销用户信息：检测token不存在': function (client) {
    client.assert.elementsCount('.page-view-usercenter .logout-box', 1);
    client.getValue('.page-view-usercenter .logout-box')


    client.execute(function(){
      // ☆ 当元素需要滑动滚动条下显示（非第一屏可视），若直接使用click点击，将不会触发点击事件(所以需要操作window小的滚动事件)
      // ☆ 被点击的元素需要出现在第一屏，才能被点击
      scrollTo(0,9000)
    },[],function(){
      client.click('.page-view-usercenter .logout-box')
      client.waitForElementPresent('.popup-box-wrapper .right-btn',500, function(){
        client.pause(2000)
        client.click('.popup-box-wrapper .right-btn');
      })
    })
    // client.refresh()
    client.pause(2000)
    client.executeAsync ( function (done) {
      done(localStorage.getItem('userInfo'))
    }, [], function (result) {
      // 用户信息已被清空, token不存在咯
      var userInfo =JSON.parse(result.value)
      client.assert.equal( _.has(userInfo, 'token') , false);
    })
  },
  '注册用户信息：': function (client) {

  }



}
