/**
 * Created by zhuxy on 2016/9/26.
 */

var URL = {};
var _ = require('underscore');

/**
 * 将参数对象转换为URL字符串
 *
 * @param {Object} query 参数对象
 * @return {string} 转换后的URL字符串，相当于`search`部分
 */
URL.serialize = function (query) {
    if (!query) {
        return '';
    }

    var search = '';
    for (var key in query) {
        if (query.hasOwnProperty(key)) {
            var value = query[key];
            // 如果`value`是数组，其`toString`会自动转为逗号分隔的字符串
            search += '&' + encode(key) + '=' + encode(value);
        }
    }

    function encode(str) {
        str = encodeURIComponent(str);
        str = str.replace('~', '%7e');
        return str;
    }

    return search.slice(1);
};
/**
 * 根据`path`和给定的`query`对象生成URL对象
 *
 * @param {string | URL} path 已经存在的URL
 * @param {Object} query URL的参数对象
 * @param {Object} [options] 控制解析行为的相关参数
 * @param {string} [options.querySeparator="~"] 用于分隔path和search的字符
 * @return {string} 处理后的URL字符串
 */
URL.withQuery = function (path, query, options) {
    path = path + '';
    var defaults = {
        querySeparator: '~'
    };
    options = _.extend(defaults, options);

    var separator = path.indexOf(options.querySeparator) < 0 ? options.querySeparator : '&';
    var search = URL.serialize(query);
    var url = path + separator + search;

    return url;
};

/**
 * url字符串转换成object对象
 *
 * @param {string} query 参数对象
 * @return {object} 转换后的object
 */
URL.parse = function (query) {
    if (!query) {
        return '';
    }

    var queryObject = {};
    var search = query.split('&');
    for (var i = 0, len = search.length; i < len; i++) {
        var searchSplit = search[i].split('=');
        var key = searchSplit[0];
        var value = searchSplit[1];
        queryObject[key] = value;
    }

    return queryObject;
};

// 更加安全的 decodee
URL.parseWithDecodeCom = function (query) {
    if (!query) {
        return '';
    }

    var queryObject = {};
    var search = query.split('&');
    for (var i = 0, len = search.length; i < len; i++) {
        var searchSplit = search[i].split('=');
        var key = searchSplit[0];
        var value = searchSplit[1];
        queryObject[key] = decodeURIComponent(value);
    }

    return queryObject;
};
module.exports= URL

