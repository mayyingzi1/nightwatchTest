/**
 * Created by zhuxy on 2016/9/21.
 * for common login and 登录成功
 */
var _ = require('underscore')
module.exports= {
  loginIn: function (client, name ,psw){
    client
      .setValue('.user-name', name)
      .setValue('.user-password', psw)
      .click('.submit-btn')
      .pause(2000)
    this.loginCheck(client)
  },
  loginCheck: function (client) {
    client
      .executeAsync ( function (done) {
        return done(localStorage.getItem('userInfo'))
      }, [], function (result) {
        var userInfo =JSON.parse(result.value)
        client.assert.equal( _.has(userInfo, 'token'), true, '检测用户信息localStorage.userInfo存在');
      })
  }
}
