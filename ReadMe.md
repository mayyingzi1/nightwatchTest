#Nightwatch#
## Get started

### Setup.
1. `git clone`.
2. Run `npm install` to get all dependencies.
3. Run `npm run selenium-setup` to install selenium.
4. Run `npm run start` 使用本机浏览器测试（默认chrome）.
6. Run `npm run start-all` 指定云测试测试多台设备且设置（"remoteTest": true ），否则将提示本地缺少相应的驱动.
5. 设置测试环境(devConfig.json)：包括浏览器、测试环境url，其否启用云测试saucelabs
    
devConfig.json
		
    {
		 // 本地测试的浏览器设置（当前支持chrome，phantomjs）
   		 "browser": "phantomjs",
		// 测试的环境设置（本地：local，内测：alpha，公测：beta，正式：product）
    	 "env": "alpha",
    	// 启用remote测试 具体配置参照nightwatch.remote.json,也可自行配置
		// 使用远端测试，目前进行测试的设置测试ie11,safari,ffox,ios,android
    	"remoteTest": true 
    }

# 科普 #
### 单元测试


- 特点：
	- 可重复运行的
	- 	持续长期有效，并且返回一致的结果
	- 	在内存中运行，没有外部依赖组件（比如说真实的数据库，真实的文件存储等）
	- 	快速返回结果
	- 	一个测试方法只测试一个问题
- 目的：
	- 确保单独的组件如预期的那样正常工作
- 工具
	- jasmine


### 集成测试
- 特点：
	- 利用真实的外部依赖（采用真实的数据库，外部的Web Service，文件存储系统等）
	- 在一个测试里面可能会多个问题（数据库正常确，配置，系统逻辑等）
	- 可以在运行较长时间之后才返回测试结果
- 目的：
	- 确保组件和组建之间的协作能正常的工作，断言点一般都是组建API, UI交互，数据库IO, 日志等
	
- 前端集成测试的工作	
	- UI模拟测试，获取钩子dom后输入输出，点击，判断前后跳是否正确
- 工具
	- 集成测试有 CasperJS/Nightwatch




## 使用Nightwatch
### 问题
- 校验localstorage的问题
	- api本身是没有localstorage的处理方法，故此，使用execute/executeAsync注入page中的数据return给client


> eg:
> 
    client.executeAsync(function(done) {
       return done(localstorage)
    }, [], function(result) {   
    	client.assert.equal(typeof result.value, "object");    
    });

- 校验click的点击的元素是窗口可视的元素
	- 因为模拟的是真是的用户操作，故此，点击的元素当不再窗口可视内容，该事件将无法捕捉到
	- 解决方案：
		- 强制将元素滚动到窗口可视 （调用window下的scrollTo，当前对于page页面的操作都是要基于execute方法）
		- 测试过程中人为的去将鼠标moveTo对应的元素（不建议使用，感觉有点违背自动化测试）

### 关于多设备的检测 （借助saucelabs 云测 自动化）
- ☆☆☆自动测试是收费的（可免费试用，时间有限90h！！！）
- 参考项目 [https://github.com/jannakha/nightwatch-saucelabs](https://github.com/jannakha/nightwatch-saucelabs)
- 设备配置（OS、Windows、Linux、Android、iOS等各种platform）
	- 相关设备的参数设置：（基于node.js）
	 [`https://wiki.saucelabs.com/display/DOCS/Platform+Configurator#/`](https://wiki.saucelabs.com/display/DOCS/Platform+Configurator#/)
- saucelabs代理服务器到本机 （可以访问内网）
	- 代理设置 [https://wiki.saucelabs.com/display/DOCS/Sauce+Connect+with+a+Proxy+Setup](https://wiki.saucelabs.com/display/DOCS/Sauce+Connect+with+a+Proxy+Setup)
	- 需要根据系统设置 tunnels cmd指令 	

>  **When running in Mac or Linux**:
			
> `bin/sc -u YOUR_USERNAME -k YOUR_ACCESS_KEY`

>  **When running in Windows:**:
			
> `bin\sc.exe -u YOUR_USERNAME -k YOUR_ACCESS_KEY`