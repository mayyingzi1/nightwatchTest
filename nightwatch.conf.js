/*
 *  Nightwatch Configuration By LancerComet at 0:51, 2016/7/17.
 *  # Carry Your World #
 *  ---
 *  Nightwatch Configuration file.
 *  Loading settings in "./build" and attach them to Nightwatch.
 *  You can check [this](http://nightwatchjs.org/guide#settings-file) to find all configuration about Nightwatch.
 */
// var process = require('process')
var path = require('path')
var seleniumConfig = require('./build/selenium-conf')
var phantomjs = require('phantomjs-prebuilt')
// 设置测试环境：包括浏览器、测试url，其否启用云测试saucelabs
// {
//   "browser": "phantomjs",// 本地测试的浏览器设置（当前支持ie，firefox，chrome，phantomjs）
//   "env": "alpha",// 测试的环境设置（本地：local，内测：alpha，公测：beta，正式：product）
//   "remoteTest": true // 开始远端测试，只要进行移动端的测试ios，Android
// }
var devConfig = require('./devConfig.json')
var remoteSauceLabs = require('./nightwatch.remote.json')

module.exports = (function (settings) {
  // Setup the browser we use.
  // We will use phantomjs by default.
  // var browser = process.argv[2] ? process.argv[2].replace(/-/g, '') : 'phantomjs'
  // var nowEnv = process.argv[3] ? process.argv[3].replace(/-/g, '') : 'product'
  var browser = devConfig.browser ? devConfig.browser : 'phantomjs';
  var nowEnv = devConfig.env ? devConfig.env : 'product';
  // var sauce = process.argv[4] ? process.argv[4].replace(/-/g, ''):'';

  var containContian ={
    local: 'http://localhost:3111/',
    alpha: 'http://192.168.1.206/imall/',
    beta: 'http://test.carisok.com/imall/',
    product: 'http://imall.carisok.com/'
  }

  if(!devConfig.remoteTest){
    if (browser === 'ie'){
      browser = 'internet explorer';
    }

    // Tell Nightwatch where is the Selenium. | 设置 Selenium 所在位置。
    settings.selenium.server_path = `${path.resolve()}/node_modules/selenium-standalone/.selenium/selenium-server/${seleniumConfig.selenium.version}-server.jar`

    // Attach Chrome web driver. | 加载 Chrome Driver
    settings.selenium.cli_args['webdriver.chrome.driver'] = `${path.resolve()}/node_modules/selenium-standalone/.selenium/chromedriver/${seleniumConfig.driver.chrome.version}-${seleniumConfig.driver.chrome.arch}-chromedriver`

    // Attach IE web driver. | 加载 IE Driver.
    settings.selenium.cli_args['webdriver.ie.driver'] = `${path.resolve()}/node_modules/selenium-standalone/.selenium/iedriver/${seleniumConfig.driver.ie.version}-${seleniumConfig.driver.ie.arch}-IEDriverServer.exe`

    // Attach Phantom.JS. | 设置 Phantom.JS.
    settings.test_settings.default.desiredCapabilities['phantomjs.binary.path'] = phantomjs.path

    // Setup browser that runs testing. | 设置使用的浏览器.
    settings.test_settings.default.desiredCapabilities.browserName = browser

    // 设置环境
    settings.test_settings.default.launch_url=containContian[nowEnv]
  }else{
    // 使用saucelabs进行多端测试
    settings = remoteSauceLabs;
    // 设置环境
    // 云测试目前只支持外网的测试，最好是beta，product
    settings.test_settings.default.launch_url=containContian[nowEnv]
    // Setup browser that runs testing. | 设置使用的浏览器.
    browser !== 'chrome' && (settings.test_settings.default.desiredCapabilities = settings.test_settings[browser]["desiredCapabilities"])
  }


  return settings
})(require('./nightwatch.json'))
